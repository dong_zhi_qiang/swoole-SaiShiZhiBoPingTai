<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 23/5/19
 * Time: 22:03
 */

namespace app\common\lib\redis;

class Predis
{
    public $redis = '';
    /**
     * 定义单列模式的变量
     * @var null
     */
    private static $_instance = null;

    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->redis = new \Redis();
        $result = $this->redis->connect(config('redis.host'), config('redis.port'), config('redis.timeOut'));
        if ($result === false) {
            throw new \Exception('redis conner error');
        }
    }

    public function set($key, $vaule, $time = 0)
    {
        if (!$key) {
            return "";
        }
        if (is_array($vaule)) {
            $vaule = json_encode($vaule);
        }
        if (!$time) {
            return $this->redis->set($key, $vaule);
        }

        return $this->redis->setex($key, $time, $vaule);

    }

    public function get($key)
    {
        if (!$key) {
            return '';
        }
        return $this->redis->get($key);
    }

//    增加有序列表方法
    public function sadd($key, $value)
    {
        return $this->redis->sAdd($key, $value);
    }

//    删除有序列表方法
    public function srem($key, $value)
    {
        return $this->redis->sRem($key, $value);
    }

//    查看有序列表方法
    public function sMembers($key)
    {
        return $this->redis->sMembers($key);
    }

//    增加删除有序列表优化方法
//    public function __call($name, $arguments)
//    {
//        // TODO: Implement __call() method.
//        if ($arguments != 2) {
//            return '';
//        }
//        return $this->redis->$name($arguments['0'], $arguments['1']);
//    }
}