<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 23/5/19
 * Time: 17:42
 */


namespace app\common\lib\util;

class Redis
{

    /**
     * 验证码前缀
     * @var string
     */
    public static $pre = "sms_";

    /**
     * 用户 user 前缀
     * @var string
     */
    public static $userpre = "user_";

    /**
     * 存储验证码
     * @param $status
     * @param string $message
     * @param array $data
     */
    public static function smskey($phome)
    {
        return self::$pre . $phome;
    }

    /**
     * 用户 key
     * @param $phome
     * @return string
     */
    public static function userkey($phome)
    {
        return self::$userpre . $phome;
    }

}