<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 23/5/19
 * Time: 16:14
 */

namespace app\common\lib\util;

class Util
{
    /**
     * api输出格式
     * @param $status
     * @param string $message
     * @param array $data
     */

    public static function show($status, $message = '', $data = [])
    {
        $result = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];

        echo json_encode($result);
    }

}