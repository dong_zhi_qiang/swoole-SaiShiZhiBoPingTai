<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 28/4/19
 * Time: 23:05
 */

namespace app\common\lib\util;


class Tool
{
    public $status;
    public $message;

    public function toJson()
    {
        echo json_encode($this, JSON_UNESCAPED_UNICODE);
    }

}