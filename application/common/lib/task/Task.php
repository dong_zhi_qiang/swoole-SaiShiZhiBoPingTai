<?php
/**
 * 代表的是 swoole里面 后续 所有 task异步 任务 都放到这里来
 * Created by PhpStorm.
 * User: kun
 * Date: 24/5/19
 * Time: 22:18
 */

namespace app\common\lib\task;

use app\common\lib\ali\Sms;
use app\common\lib\redis\Predis;
use app\common\lib\util\Redis;

class Task
{
    /**
     * 异步发红验证码
     * @param $data
     * @param $sver swoole 对象
     */
    public function sendSms($data, $sver)
    {
        try {
            $response = Sms::sendSms($data['phone'], $data['code']);
        } catch (\Exception $e) {
//            return app\common\lib\util\Util::show(config('code.error'), '阿里大于内部异常');
//            echo $e->getMessage();
            return false;
        }
//        //异步redis
//        if ($response->Code === 'OK') {
//            $redis = new \Swoole\Coroutine\Redis();
//            $redis->connect(config('redis.host'), config('redis.port'));
//            $redis->set(Redis::smskey($data['phone']), $data['code'], config('redis.out_time'));
//            return Util::show(config('code.success'), 'success');
//        } else {
//            return Util::show(config('code.error'), '验证码发送失败');
//        }


//        同步redis 放送成功存入redis
        if ($response->Code === 'OK') {
            Predis::getInstance()->set(Redis::smskey($data['phone']), $data['code'], config('redis.out_time'));
        } else {
            return false;
        }
//        print_r($response);
        return true;
    }


    /**
     * 通过task机制发送赛况实时数据给客户端
     * @param $data
     * @param $sver swoole 对象
     */
    public function pushLive($data, $sver)
    {
        //        获取链接用户
//        赛事的基本情况入库  数据组织好 push到直播平台
        $clients = Predis::getInstance()->sMembers(config('redis.live_game_key'));
        foreach ($clients as $fd) {
            $sver->push($fd, json_encode($data));
        }
    }

}