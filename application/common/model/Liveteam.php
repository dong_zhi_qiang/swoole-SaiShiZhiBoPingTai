<?php

namespace app\common\model;

use think\Model;

class Liveteam extends Model
{
    protected $pk = 'id';
    protected $table = 'liveteam';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
