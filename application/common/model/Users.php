<?php

namespace app\common\model;

use think\Model;

class Users extends Model
{
    protected $pk = 'id';
    protected $table = 'users';
    protected $createTime = 'users_create_time';
    protected $updateTime = 'users_update_time';
}
