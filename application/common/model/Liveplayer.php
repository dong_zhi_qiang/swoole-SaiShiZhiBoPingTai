<?php

namespace app\common\model;

use think\Model;

class Liveplayer extends Model
{
    protected $pk = 'id';
    protected $table = 'liveplayer';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
