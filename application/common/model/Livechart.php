<?php

namespace app\common\model;

use think\Model;

class Livechart extends Model
{
    protected $pk = 'id';
    protected $table = 'livechart';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
