<?php

namespace app\common\model;

use think\Model;

class Livegame extends Model
{
    protected $pk = 'id';
    protected $table = 'livegame';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
