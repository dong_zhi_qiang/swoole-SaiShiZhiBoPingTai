<?php

namespace app\common\model;

use think\Model;

class Admin extends Model
{
    protected $pk = 'id';
    protected $table = 'admin';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
