<?php

namespace app\common\model;

use think\Model;

class Liveouts extends Model
{
    protected $pk = 'id';
    protected $table = 'liveouts';
    protected $createTime = 'liveteam_create_time';
    protected $updateTime = 'liveteam_update_time';
}
