<?php

namespace app\index\controller;


use app\common\lib\redis\Predis;
use app\common\lib\util\Redis;
use app\common\lib\util\Util;

class Login
{
    /**
     * 登陆认证
     */
    public function index()
    {
        $phoneNum = intval($_GET['phone_num']);
        $code = intval($_GET['code']);
        if (empty($phoneNum) || empty($code)) {
            return Util::show(config('code.error'), 'phone or code is error');
        }


        try {
            $redisCode = Predis::getInstance()->get(Redis::smskey($phoneNum));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        if ($redisCode == $code) {
            $data = [
                'user' => $phoneNum,
                'srcKey' => md5(Redis::userkey($phoneNum)),
                'time' => time(),
                'isLogin' => true,
            ];
            Predis::getInstance()->set(Redis::userkey($phoneNum), $data);
            return Util::show(config('code.success'), '登陆成功', $data);
        } else {
            return Util::show(config('code.error'), '验证码错误');
        }
    }
}
