<?php

namespace app\index\controller;


use app\common\lib\util\Util;

class Chart
{
    public function index()
    {
//        判断用户登陆。。。。。。

        if (empty($_POST['game_id'])) {
            return Util::show(config('code.error'), '信息发送了失败');
        }
        if (empty($_POST['content'])) {
            return Util::show(config('code.error'), '信息内容为空');
        }

        $data = [
            'user' => '用户' . rand(0, 2000),
            'content' => $_POST['content']
        ];

        foreach ($_POST['http_server']->ports[1]->connections as $fd) {
            $_POST['http_server']->push($fd, json_encode($data));
        }

        return Util::show(config('code.success'), '信息发送了成功', $data);
    }


}
