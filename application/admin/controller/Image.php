<?php

namespace app\admin\controller;


use app\common\lib\util\Util;

class Image
{
    public function index()
    {
        $file = request()->file('file');
        $path = '../public/static/upload';
        $info = $file->move($path);
        if ($info) {
            $data = [
                'image' => config('live.host') . '/upload/' . $info->getSaveName(),
            ];
            return Util::show(config('code.success'), 'ok', $data);
        } else {
            return Util::show(config('code.error'), 'error');
        }

    }

}
