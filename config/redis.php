<?php
/**
 * redis 相关配置
 * Created by PhpStorm.
 * User: kun
 * Date: 23/5/19
 * Time: 16:31
 */

return [
    'host' => '127.0.0.1',
    'port' => 6379,
    'out_time' => 120,
    'timeOut' => 5,  //链接超时时间
    'live_game_key' => 'live_game_key'
];