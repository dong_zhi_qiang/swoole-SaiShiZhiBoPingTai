<?php
/**
 * Created by PhpStorm.
 * User: kun
 * Date: 23/5/19
 * Time: 23:36
 */

class Http
{
    CONST HOST = "0.0.0.0";
    CONST PORT = 8811;
    public $http = null;

    public function __construct()
    {
        $this->http = new swoole_http_server(self::HOST, self::PORT);

        $this->http->set(
            [
                'enable_static_handler' => true,
                'document_root' => "/www/wwwroot/zb.miaosuwulimi.cn/public/static/",
                'worker_num' => 4,
                'task_worker_num' => 4,
            ]
        );

        $this->http->on('workerstart', [$this, 'onWorkerStart']);
        $this->http->on('request', [$this, 'onRequest']);
        $this->http->on('task', [$this, 'onTask']);
        $this->http->on('finish', [$this, 'onFinish']);
        $this->http->on('close', [$this, 'onClose']);
        $this->http->start();
    }

    /**
     * orkerStart 回调
     * @param swoole_server $server
     * @param $worker_id
     */
    public function onWorkerStart(swoole_server $server, $worker_id)
    {
        //只加载一次框架
        // 定义应用目录
        define('APP_PATH', __DIR__ . '/../application/');
        //  加载框架引导文件
        // 加载基础文件
//        require __DIR__ . '/../thinkphp/base.php';
        require __DIR__ . '/../thinkphp/start.php';
    }

    /**
     * $request 请求回调
     * @param $request 请求信息
     * @param $response 返回信息
     */
    public function onRequest($request, $response)
    {
        //        缺点 每次都要重新加载框架
//    define('APP_PATH', __DIR__ . '/../application/');
//    require_once __DIR__ . '/../thinkphp/base.php';

        $_SERVER = [];
        if (isset($request->server)) {
            foreach ($request->server as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
        if (isset($request->header)) {
            foreach ($request->header as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
        $_GET = [];
        if (isset($request->get)) {
            foreach ($request->get as $k => $v) {
                $_GET[$k] = $v;
            }
        }
        $_POST = [];
        if (isset($request->post)) {
            foreach ($request->post as $k => $v) {
                $_POST[$k] = $v;
            }
        }
        $_POST['http_server'] = $this->http;
        ob_start();
        // 执行应用并响应
        try {
            think\Container::get('app', [APP_PATH])
                ->run()
                ->send();
        } catch (\Exception $e) {
            //todo
        }

        $res = ob_get_contents();
        ob_end_clean();
        $response->end($res);
//    $http->close();


    }

    /**
     * @param $serv
     * @param $taskId
     * @param $workweId
     * @param $data
     */
    public function onTask($serv, $taskId, $workweId, $data)
    {
        //        分发task任务机制，让不同的任务走不同的逻辑
        $obj = new app\common\lib\task\Task;
        $method = $data['method'];
        $flag = $obj->$method($data['data']);

        return $flag;
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     */
    public function onFinish($serv, $taskId, $data)
    {
//        print_r($serv);
        echo "taskId:{$taskId}\n";
        echo "finish-data-success:{$data}\n";
    }


    /**
     * 监听ws消息事件结束
     * @param $ws
     * @param $fd
     */
    public function onClose($ws, $fd)
    {
        echo "clientid:{$fd}\n";
    }


}


$odj = new Http();










