<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Liveouts extends Migrator
{
    /**
     *赛事的赛况表
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('liveouts');
        $table
            ->addColumn('game_id', 'integer', array('limit' => 15, 'default' => 0, 'comment' => '赛事id'))
            ->addColumn('team_id', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '球队id'))
            ->addColumn('content', 'text', array('comment' => '解说'))
            ->addColumn('image', 'string', array('limit' => 32, 'default' => '', 'comment' => '赛况图片'))
            ->addColumn('type', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '赛况'))
            ->addColumn('status', 'integer', array('default' => 0, 'comment' => '最后登录时间'))
            ->addColumn('is_delete', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '删除状态，1已删除'))
            ->addColumn('liveouts_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('liveouts_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
