<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Admin extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('admin');
        $table
            ->addColumn('username', 'string', array('default' => '幸福的小海豚', 'comment' => '用户名'))
            ->addColumn('password', 'string', array('comment' => '密码'))
            ->addColumn('admin_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('admin_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();

    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
