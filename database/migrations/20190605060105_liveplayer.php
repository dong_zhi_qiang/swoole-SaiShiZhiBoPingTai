<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Liveplayer extends Migrator
{
    /**
     * // 球员表
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('liveplayer');
        $table->addColumn('name', 'string', array('limit' => 15, 'default' => '', 'comment' => '球员名'))
            ->addColumn('image', 'string', array('limit' => 50, 'default' => '', 'comment' => '球员图片'))
            ->addColumn('age', 'integer', array('limit' => 12, 'default' => 0, 'comment' => '球员年龄'))
            ->addColumn('position', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '球员位置'))
            ->addColumn('status', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '球员状态'))
            ->addColumn('liveplayer_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('liveplayer_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
