<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Livegame extends Migrator
{
    /**
     * // 直播表
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('livegame');
        $table
            ->addColumn('a_id', 'integer', array('limit' => 15, 'default' => 0, 'comment' => 'a球队id'))
            ->addColumn('b_id', 'integer', array('limit' => 15, 'default' => 0, 'comment' => 'b球队id'))
            ->addColumn('a_score', 'integer', array('limit' => 15, 'default' => 0, 'comment' => 'a球队得分'))
            ->addColumn('b_score', 'integer', array('limit' => 15, 'default' => 0, 'comment' => 'a球队得分'))
            ->addColumn('narrators', 'text', array('comment' => '幕后解说'))
            ->addColumn('start_time', 'timestamp', array('default' => 0, 'comment' => '解说时间'))
            ->addColumn('status', 'boolean', array('limit' => 1, 'default' => 0, 'comment' => '职位'))
            ->addColumn('livegame_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('livegame_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}

