<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Liveteam extends Migrator
{
    /**
     * // 球队表
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('liveteam');
        $table
            ->addColumn('name', 'string', array('limit' => 25, 'default' => '', 'comment' => '球队名'))
            ->addColumn('image', 'string', array('limit' => 50, 'default' => '', 'comment' => '球队图片'))
            ->addColumn('type', 'integer', array('limit' => 1, 'default' => 0, 'comment' => '球队状态'))
            ->addColumn('liveteam_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('liveteam_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
