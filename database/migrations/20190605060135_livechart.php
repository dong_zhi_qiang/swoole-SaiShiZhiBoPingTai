<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Livechart extends Migrator
{
    /**
     * 聊天室表
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('livechart');
        $table
            ->addColumn('game_id', 'integer', array('limit' => 15, 'default' => 0, 'comment' => '赛事id'))
            ->addColumn('user_id', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '用户id'))
            ->addColumn('content', 'text', array('comment' => '内容'))
            ->addColumn('status', 'integer', array('limit' => 10, 'default' => 0, 'comment' => '状态'))
            ->addColumn('livechart_create_time', 'timestamp', ['comment' => '添加时间', 'default' => 0])
            ->addColumn('livechart_update_time', 'timestamp', ['comment' => '修改时间', 'default' => 0])
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
