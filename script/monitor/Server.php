<?php
/**
 * 监控服务 ws http 8811
 * Created by PhpStorm.
 * User: kun
 * Date: 4/6/19
 * Time: 18:53
 */

use app\common\lib\ali\Sms;

class Server
{
    const PORT = 8811;


    public function port()
    {
        $shell = "netstat -anp 2>/dev/null | grep 8811 | grep LISTEN | wc -l";

        $result = shell_exec($shell);
        if ($result != 1) {
            // 发送报警服务 邮件 短信
            /// todo
            ///  Sms::sendSms(config('phone.phone'), 'ws服务停止');
            echo date("Ymd H:i:s") . "error" . PHP_EOL;
        } else {
            echo date("Ymd H:i:s") . "succss" . PHP_EOL;
        }
    }

}

swoole_timer_tick(2000, function ($timer_id) {
    (new Server())->port();
    echo "time-start" . PHP_EOL;
});



//nohup  /usr/bin/php  /www/wwwroot/zb.miaosuwulimi.cn/script/monitor/Server.php  > /www/wwwroot/zb.miaosuwulimi.cn/script/monitor/start.txt &


//nohup  /usr/bin/php  /www/wwwroot/zb.miaosuwulimi.cn/script/ws.php  > /www/wwwroot/zb.miaosuwulimi.cn/script/monitor/ws.txt &